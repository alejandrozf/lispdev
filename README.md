How to use this enviroment:

mkdir /home/alejandrozf/lisp
cp Dockerfile-ecl-quicklisp Dockerfile

On shell:
$ docker-compose up -d
$ docker exec -it lispdev ecl

On REPL
* (ql:quickload :swank)
* (swank:create-server)

On another shell tab:
ssh -L4005:127.0.0.1:4005 172.17.0.1

On Emacs
M-x slime-connect RET localhost RET 4005

Enjoy!

Note: you must be sure that you have the same Slime version in both host & lisp dev-env container
